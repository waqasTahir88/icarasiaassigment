//
//  AppDelegate.h
//  ICarAsiaAssigment
//
//  Created by Waqas Tahir on 20/01/2017.
//  Copyright © 2017 Waqas Tahir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

