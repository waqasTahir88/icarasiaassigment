//
//  CAJsonViewController.m
//  ICarAsiaAssigment
//
//  Created by Waqas Tahir on 26/01/2017.
//  Copyright © 2017 Waqas Tahir. All rights reserved.
//

#import "CAJsonViewController.h"
#import "CAChatManager.h"

@interface CAJsonViewController ()
@property (nonatomic, strong) CAChatManager *chatManager;
@end

@implementation CAJsonViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    __weak typeof(self) weakSelf = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    self.title = @"Json Results";
    self.chatManager = [CAChatManager sharedManager];
    [self.chatManager setDataUpdateHandler:^{
        [weakSelf.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.chatManager totalJson];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"jsonCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.chatManager jsonAtIndex:indexPath.row];
    
    return cell;
}

@end
