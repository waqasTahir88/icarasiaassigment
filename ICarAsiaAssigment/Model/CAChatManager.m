//
//  CAChatManager.m
//  ICarAsiaAssigment
//
//  Created by Waqas Tahir on 24/01/2017.
//  Copyright © 2017 Waqas Tahir. All rights reserved.
//

#import "CAChatManager.h"
#import "CAConstants.h"

@interface CAChatManager ()
@property (nonatomic, strong) NSMutableArray *messagesList;
@property (nonatomic, strong) NSMutableArray *jsonList;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@end

@implementation CAChatManager
static CAChatManager* chatManager;

#pragma mark Private Methods
+ (CAChatManager*)sharedManager {
    //==========================================================
    // THE SINGLETON CLASS
    //==========================================================
    if (!chatManager) {
        chatManager = [[CAChatManager alloc] init];
    }
    return chatManager;
}

- (instancetype)init {
    if ([super init]) {
        [self setMsgBubbleColor];
    }
    return self;
}

- (void)setMsgBubbleColor {
    //==========================================================
    // CREATE MESSAGE BUBBLE IMAGES OBJECTS
    //==========================================================
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
}


- (void)saveJsonForMsg:(NSString*)message textResult:(NSMutableArray*)textResultList {
    
    //==========================================================
    // IF TEXT CONTAIN CARLIST.MY URL THEN THIS IF IS EXECUTED
    //==========================================================
    if (textResultList.count > 0) {
        __weak typeof(self) weakSelf = self;
        __block NSInteger textResultCount = 0;
        __block NSMutableArray *linkArray = [NSMutableArray arrayWithCapacity:5];
        __block NSString *chatMsg = message;
        NSInteger offset = 0;
        //==========================================================
        // LOOPING THE RESULT WHICH CONTAIN THE CARLIST.MY URL
        //==========================================================
        for (NSTextCheckingResult *textResult in textResultList) {
            NSTextCheckingResult *newResult = [textResult resultByAdjustingRangesWithOffset: offset];
            chatMsg = [message stringByReplacingCharactersInRange:newResult.range withString:@""];
            
            offset =  offset - newResult.range.length;
            message = chatMsg;
            NSURL *url = textResult.URL;
            //==========================================================
            // ASYN FETCHING THE TITLE OF THE URL PAGE
            //==========================================================
            [NSThread detachNewThreadWithBlock:^{
                NSError *error;
                NSString *htmlString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
                NSString *title = [weakSelf scanString:htmlString startTag:@"<title>" endTag:@"</title>"];
                NSLog(@"%@", title);
                textResultCount ++;
                NSDictionary *linkDict = @{@"url": textResult.URL.absoluteString,
                                           @"title": title};
                [linkArray addObject:linkDict];
                if (textResultCount == textResultList.count) {
                    //==========================================================
                    // WHEN ALL TITLE OF CARLIST.MY URLS ARE FETCHED WE PASS THE MSG AND ARRAY TO DIC CONTAING URL AND TITLE
                    //==========================================================
                    [weakSelf createJsonDicWith:chatMsg urlArray:linkArray];
                }
            }];
        }
    }
    else {
        //==========================================================
        // IF TEXT CONTAIN NO CARLIST.MY URL THEN THIS ELSE IS EXECUTED
        //==========================================================
        [self createJsonDicWith:message urlArray:nil];
    }
}

- (void)createJsonDicWith:(NSString*)msg urlArray:(NSArray*)urlList {
    //==========================================================
    // IF URLLIST IS NIL THE JSON MADE IS "{MESSAGE: STRING }" ELSE  {"MESSAGE":STRING", "LINK":{URL: STRING, TITLE:STRING}}
    //==========================================================
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:msg forKey:@"message"];
    if (urlList) {
        [jsonDict setValue:urlList forKey:@"links"];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self.jsonList addObject:jsonStr];
    //==========================================================
    // THIS BLOCK IS EXECTED IN CAJSONVIEWCONTROLLER TO RELOAD TABLE VIEW
    //==========================================================
    if (self.dataUpdateHandler != nil) {
        self.dataUpdateHandler();    
    }
    
    
}

- (NSString *)scanString:(NSString *)string
       startTag:(NSString *)startTag
         endTag:(NSString *)endTag
{
    //==========================================================
    // SCANS THE HTML PAGE AND FIND TEXT WITH TITLE TAG
    //==========================================================
    NSString* scanString = @"";
    
    if (string.length > 0) {
        
        NSScanner* scanner = [[NSScanner alloc] initWithString:string];
        
        @try {
            [scanner scanUpToString:startTag intoString:nil];
            scanner.scanLocation += [startTag length];
            [scanner scanUpToString:endTag intoString:&scanString];
        }
        @catch (NSException *exception) {
            return nil;
        }
        @finally {
            return scanString;
        }
        
    }
    
    return scanString;
    
}

#pragma mark Public Methods
/**
 retutrn the json string used to tableview of CAJsonViewController
 */
- (NSString*)jsonAtIndex:(NSInteger)index {
    return [self.jsonList objectAtIndex:index];
}

/**
 retutrn the JSQMessage used to collectionview of CAJsonViewController
 */
- (JSQMessage*)msgAtIndex:(NSInteger)index {
    if (self.messagesList.count > index) {
        return [self.messagesList objectAtIndex:index];
    }
    return nil;
}
/**
 Helper to add jsqMessages
 */
- (void)addMsg:(JSQMessage*)msg {
    [self.messagesList addObject:msg];
}

/**
 To add chat messge into message array. This methd blacklist certains parts of the chat i.e phone number and any other URL than carlist.my
 NOTE:
 It is using NSDataDetector for finding the URL and NSRegularExpression to find the phone 
 This code is optimised to enter multiple URL and Phone
 
 Test case used are
 - I have another car at  http://www.example.com/listing10.htm“
 - Hi Lee, can  you call me at +60175570098
 - You can find the listing at https://www.carlist.my/used-cars/3300445/2011-toyota-vios-1-5-trd-sportivo-33-000km-full-toyota-serviced-record-like-new-11/
 - www.google.com is huge company but facebook.com is more engaging for people my site is carlist.my
 */
- (void)addMsgWithText:(NSString*)text senderId:(NSString*)senderId displayName:(NSString*)displayName  date:(NSDate*)date {
    
    __block NSString *textStr = text;
    NSError *error;
    NSMutableArray *textResultList = [NSMutableArray arrayWithCapacity:10];
    __block NSInteger offset = 0;
    //==========================================================
    // FINDING THE URL FROM TEXT USING DATA-DETECTOR ITS REPLACE URL OTHER THE CARLIST.MY WITH *
    // TO HANDLE MUTIPLE URL IN SINGLE TEXT WE HAVE USED "resultByAdjustingRangesWithOffset"
    // TO HANDLE URL TYPED URL WITHOUT HTTP WE HAVE USED "stringByReplacingCharactersInRange"
    //==========================================================
    NSDataDetector *urlDetector = [[NSDataDetector alloc] initWithTypes: NSTextCheckingTypeLink error:&error];
    [urlDetector enumerateMatchesInString:text options:kNilOptions range:NSMakeRange(0, text.length) usingBlock:^(NSTextCheckingResult * _Nullable result, NSMatchingFlags flags, BOOL * _Nonnull stop) {
        NSLog(@"Match is : %@", result);
        NSTextCheckingResult *newResult = [result resultByAdjustingRangesWithOffset: -offset];
        if (![[newResult.URL.host lowercaseString] containsString:@"carlist.my"]) {
            textStr = [textStr stringByReplacingCharactersInRange:newResult.range withString:@"*****"];
            offset = result.range.length - kSTAR_COUNT_IN_URL + offset;
            
        }
        else {
            [textResultList addObject:newResult];
        }
    }];
    
    //==========================================================
    // FINDING THE PHONE OF +AABBCCCDDDD FORMAT FROM TEXT USING REGULAR EXPRESSION ITS REPLACE PHONE WITH *
    //==========================================================
    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:kPHONE_REGEX
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:&error];
    
    NSArray *matches = [regex matchesInString:textStr options:0 range:NSMakeRange(0, [textStr length])];
    for (NSTextCheckingResult *result in matches) {
        NSLog(@"result: %@", result);
        textStr = [textStr stringByReplacingCharactersInRange:result.range withString:[[NSString string] stringByPaddingToLength:result.range.length withString:@"*" startingAtIndex:0]];
        
    }
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:displayName
                                                          date:date
                                                          text:textStr];
    [self.messagesList addObject:message];
    
    //==========================================================
    // CALLING IT TO MAKE JSON OF TEXT
    //==========================================================
    [self saveJsonForMsg:textStr textResult:textResultList];
}

/**
 To remove chat messge which also delete json
 */
- (void)removeMsg:(JSQMessage*)msg {
    if ([self.messagesList containsObject:msg]) {
        NSUInteger index = [self.messagesList indexOfObject:msg];
        [self.messagesList removeObject:msg];
        [self.jsonList removeObjectAtIndex:index];
    }
}

/**
 To remove chat messge which also delete json
 */
- (void)removeMsgAtIndex:(NSInteger)index {
    if (self.messagesList.count > index) {        
        [self.messagesList removeObjectAtIndex:index];
        [self.jsonList removeObjectAtIndex:index];
    }
}

/**
 return toatal messages array count used in CAChatViewController
 */
- (NSInteger)totalMsgs {
    return self.messagesList.count;
}

/**
 return toatal json array count used in CAJsonViewController
 */
- (NSInteger)totalJson {
    return self.jsonList.count;
}

/**
 return bubble color of outgoing msg
 */
- (JSQMessagesBubbleImage*)outgoingBubbleColor {
    return self.outgoingBubbleImageData;
}

/**
 return bubble color of incoming msg
 */
- (JSQMessagesBubbleImage*)incomingBubbleColor {
    return self.incomingBubbleImageData;
}

#pragma mark Accessor Methods
/**
    getter of messagesList array
 */
- (NSMutableArray*)messagesList {
    if (!_messagesList) {
        _messagesList = [[NSMutableArray alloc] init];
    }
    return _messagesList;
}

/**
    getter of jsonList array
 */
- (NSMutableArray*)jsonList {
    if (!_jsonList) {
        _jsonList = [[NSMutableArray alloc] init];
    }
    return _jsonList;
}
@end
