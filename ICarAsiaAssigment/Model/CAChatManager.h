//
//  CAChatManager.h
//  ICarAsiaAssigment
//
//  Created by Waqas Tahir on 24/01/2017.
//  Copyright © 2017 Waqas Tahir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessages.h"

@interface CAChatManager : NSObject
+ (CAChatManager*)sharedManager;
@property (copy) void (^dataUpdateHandler)();

- (NSString*)jsonAtIndex:(NSInteger)index;
- (JSQMessage*)msgAtIndex:(NSInteger)index;
- (void)removeMsgAtIndex:(NSInteger)index;
- (void)addMsg:(JSQMessage*)msg;
- (void)addMsgWithText:(NSString*)text senderId:(NSString*)senderId displayName:(NSString*)displayName  date:(NSDate*)date;
- (void)removeMsg:(JSQMessage*)msg;
- (NSInteger)totalMsgs;
- (NSInteger)totalJson;
- (JSQMessagesBubbleImage*)outgoingBubbleColor;
- (JSQMessagesBubbleImage*)incomingBubbleColor;
@end
