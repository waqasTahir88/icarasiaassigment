//
//  main.m
//  ICarAsiaAssigment
//
//  Created by Waqas Tahir on 20/01/2017.
//  Copyright © 2017 Waqas Tahir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
